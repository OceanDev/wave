const express = require('express')

const AnswerRouter = express.Router()

function find(predicate) {
  return new Promise((resolve, reject) => {
    answersColl.find(predicate).toArray((err, docs) => {
      if (err) return reject(err)
      return resolve(docs)
    })
  })
}

AnswerRouter.route('/answer').get(async (req, res) => {
  const results = await find({
    text: req.query.text || ''
  })
  if (results.length === 0) {
    return res.status(404).json({
      status: 404,
      message: 'not found in database'
    })
  }
  return res.status(200).json({
    status: 200,
    message: 'found',
    answer: results[0]
  })
})

module.exports = AnswerRouter
