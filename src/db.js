const { MongoClient } = require('mongodb')

module.exports = function connect() {
  return new Promise((resolve, reject) => {
    MongoClient.connect(process.env.MONGO_URI, (err, res) => {
      if (err) return reject(err)
      return resolve(res)
    })
  })
}
