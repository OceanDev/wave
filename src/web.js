const express = require('express'),
  helmet = require('helmet'),
  cors = require('cors')
const app = express()
app.use(cors())
app.use(helmet())

app.use('/answers', require('./routes/AnswerRouter'))

app.get('/', (req, res) => {
  res.format({
    text: () => {
      res.send(
        'Welcome to wave.ocean.rip\nPerhaps you may have meant to go to https://ocean.rip.'
      )
    },
    html: () => {
      res.send(
        '<!DOCTYPE html><html lang="en"><head><meta http-equiv="refresh" content="10;url=https://ocean.rip" /><title>Redirecting...</title></head><body><h1>Welcome to wave.ocean.rip</h1><h2>Redirecting to ocean.rip in 10 seconds.</h2><a href="https://ocean.rip">Click here if you are not automatically redirected</a></body></html>'
      )
    },
    json: () => {
      res.json({
        status: 200,
        message: 'Welcome to wave.ocean.rip'
      })
    }
  })
})

app.use((req, res, next) => {
  res.status(404).json({
    status: 404,
    message: 'Page not found'
  })
})

module.exports = function listen() {
  return new Promise((resolve, reject) => {
    // listen
    app.listen(process.env.PORT || 3000, () => {
      console.log('Listening on port', process.env.PORT || 3000)
      resolve()
    })
  })
}
