require('dotenv').config() // populate process.env with .env file

const connect = require('./db'),
  listen = require('./web')

connect()
  .then(client => {
    global.answersColl = client.db('imposter').collection('answers2')
    listen()
      .then(() => {
        console.log('Ready for connections.')
      })
      .catch(err => {
        console.error('Failed to listen on port', process.env.PORT)
        console.error(err)
        process.exit(1)
      })
  })
  .catch(err => {
    console.error('Failed to connect to the database!', err)
    process.exit(1)
  })
